import { SignerWithAddress } from '@nomiclabs/hardhat-ethers/dist/src/signer-with-address';
import { expect } from 'chai';
import { deployments, ethers, getNamedAccounts } from 'hardhat';
import { FT } from './../typechain/FT.d';

// Tip: Chain state will be reset after each test !

describe("FT", function() {
  let deployer: string, signers: SignerWithAddress[], fooAcc: SignerWithAddress, FT: FT
  before(async () => {
    deployer = (await getNamedAccounts()).deployer;
    signers = (await ethers.getSigners());
    fooAcc = signers[1];
    
    FT = await ethers.getContract('FT', deployer) as FT;
  });
  beforeEach(async () => {
    // This will init/reset contract state before each test
    await deployments.fixture();
  });

  it("Should return correct totalSupply", async function () {
    const totalSupply = (await FT.totalSupply());
    expect(totalSupply).to.equal(ethers.constants.WeiPerEther.mul(126));
    const ownerBalance = await FT.balanceOf(deployer);
    expect(totalSupply).to.equal(ownerBalance);
  });
  it("Minter should own totalSupply", async function () {
    const totalSupply = (await FT.totalSupply());
    const ownerBalance = await FT.balanceOf(deployer);
    expect(totalSupply).to.equal(ownerBalance);
  });

  it("Should allow to transfer", async function () {
    const amount = ethers.constants.WeiPerEther;
   
    const deployerBalanceBefore = (await FT.balanceOf(deployer));
    console.debug("deployer balance", deployerBalanceBefore.div(ethers.constants.WeiPerEther).toString())
    FT.transfer(fooAcc.address, amount) // 1.0

    const deployerBalanceAfter = (await FT.balanceOf(deployer));
    const balanceAfter = (await FT.balanceOf(fooAcc.address));
    console.debug("deployer balance after", deployerBalanceAfter.div(ethers.constants.WeiPerEther).toString())
    console.debug("ownerA balance after", balanceAfter.div(ethers.constants.WeiPerEther).toString())
    expect(deployerBalanceAfter).to.equal(deployerBalanceBefore.sub(amount))
    expect(balanceAfter).to.equal(amount)
  });

  it("Should burn correctly", async function () {
    // FT.transfer(tokenOwner, ethers.constants.WeiPerEther) // 1.0
    const balanceBefore = (await FT.balanceOf(deployer)).div(ethers.constants.WeiPerEther);
    console.debug("deployer token balance", balanceBefore.toString())
    
    let result = await FT.burn(ethers.constants.WeiPerEther);
    expect(result).to.be.ok;
    const balanceAfter = (await FT.balanceOf(deployer)).div(ethers.constants.WeiPerEther);
    // console.debug("burn 1 full - ", result)
    console.debug("deployer token balance after", balanceAfter.toString())
    expect(balanceAfter).to.eq(balanceBefore.sub(1))
  });
});
