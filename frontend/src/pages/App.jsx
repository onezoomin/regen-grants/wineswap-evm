import { Web3Provider } from '@ethersproject/providers'
import { createWeb3ReactRoot, Web3ReactProvider } from '@web3-react/core'

import { HashRouter, Redirect, Route, Switch } from 'react-router-dom'
import Web3ReactManager from '../components/Web3ReactManager'
import { EthBlockProvider } from '../components/Web3ReactManager/block-listener'
import AppProvider from '../context'
import EthProvider from '../context/eth-data'
import { useResizeFix } from '../hooks/misc'
import { NetworkContextName } from '../utils/constants'
import UIProvider from './Body/UIContext'
import Main from './Main'

// const { REACT_APP_MORALIS_APPID: MORALIS_APPID, REACT_APP_MORALIS_URL: MORALIS_URL } = process.env

let Web3ProviderNetwork
try {
  Web3ProviderNetwork = createWeb3ReactRoot(NetworkContextName)
} catch (e) {
  // happens in some hot reload situations
  if (e.message.includes('A root already exists for provided key')) window.location.reload()
  else throw e
}
if (window.ethereum) {
  window.ethereum.autoRefreshOnNetworkChange = false
}

function getLibrary (provider) {
  const library = new Web3Provider(provider, 'any')
  library.pollingInterval = 12000 // just below most block times on eth homestead (june 21)
  return library
}
export default function App () {
  useResizeFix()

  return (
    <AppProvider>
      <UIProvider>
        <Web3ReactProvider getLibrary={getLibrary}>
          <Web3ProviderNetwork getLibrary={getLibrary}>
            <Web3ReactManager>
              <EthBlockProvider>
                <EthProvider>
                  <HashRouter>
                    <Switch>
                      <Route exact strict path="/" render={() => <Main />} />
                      <Route exact strict path="/status" render={() => <Main status />} />
                      <Route exact strict path="/faq" render={() => <Main faq />} />
                      <Redirect to="/" />
                    </Switch>
                  </HashRouter>
                </EthProvider>
              </EthBlockProvider>
            </Web3ReactManager>
          </Web3ProviderNetwork>
        </Web3ReactProvider>
      </UIProvider>
    </AppProvider>)
}
