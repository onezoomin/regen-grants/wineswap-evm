
import get from 'lodash/get'

/** Either error.message or JSON representation */
export function formatError (error: Error|object) {
  let msgOrString = get(error, 'message', JSON.stringify(error, null, 2))
  if (msgOrString !== null && typeof msgOrString !== 'string') msgOrString = JSON.stringify(msgOrString, null, 2) // sometimes, error.message is not a string........
  return msgOrString
}
