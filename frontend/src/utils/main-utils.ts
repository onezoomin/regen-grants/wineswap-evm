import { BigNumber } from '@ethersproject/bignumber'
import { AddressZero } from '@ethersproject/constants'
import { Contract } from '@ethersproject/contracts'
import { JsonRpcSigner, Web3Provider } from '@ethersproject/providers'
import { Fraction, Price, Token, TokenAmount, WETH9 } from '@uniswap/sdk-core'
import { ethers } from 'ethers'
import ERC20_ABI from './erc20.json'
import UNIWINE_ABI from './uniwine-abi.json'

export const FT_NAME = 'VINO'
export const NFT_NAME = 'VERITAS'

export const INITIAL_SUPPLY = 108 // TODO: magic number - contract value?

export const ROUTER_ADDRESS = {
  mainnet: '0x7a250d5630B4cF539739dF2C5dAcb4c659F2488D', // https://uniswap.org/docs/v2/smart-contracts/factory/
  rinkeby: '0x7a250d5630B4cF539739dF2C5dAcb4c659F2488D', // yup, they're the same
  goerli: '0x7a250d5630B4cF539739dF2C5dAcb4c659F2488D',
}
// const FACTORY_ADDRESS = {
//   mainnet: '0x5C69bEe701ef814a2B6a3EDD4B1652CB9cc5aA6f', // https://uniswap.org/docs/v2/smart-contracts/factory/
//   rinkeby: '0x5C69bEe701ef814a2B6a3EDD4B1652CB9cc5aA6f', // yup, they're the same
//   goerli: '0x5C69bEe701ef814a2B6a3EDD4B1652CB9cc5aA6f',
// }

const EXPLORER_URLS = {
  mainnet: 'https://etherscan.io/',
  rinkeby: 'https://rinkeby.etherscan.io/',
  goerli: 'https://goerli.etherscan.io/',
}

export interface AddressMap {
  [key: string]: string
}
export interface NetworkAddressMap {
  [key: string]: AddressMap
}
export const TOKEN_ADDRESSES: NetworkAddressMap = {
  mainnet: {
    ETH: WETH9[1].address,
    WINE: 'TODO',
    SOCKS: '0x23B608675a2B2fB1890d3ABBd85c5775c51691d5', // TODO: check this... and the others
    ANT: '0x960b236A07cf122663c4303350609A66A7B288C0',
    BAT: '0x0D8775F648430679A709E98d2b0Cb6250d2887EF',
    DAI: '0x6B175474E89094C44Da98b954EedeAC495271d0F',
    KNC: '0xdd974D5C2e2928deA5F71b9825b8b646686BD200',
    MKR: '0x9f8F72aA9304c8B593d555F12eF6589cC3A579A2',
    RDN: '0x255Aa6DF07540Cb5d3d297f0D0D4D84cb52bc8e6',
    REP: '0x1985365e9f78359a9B6AD760e32412f4a445E862',
    SNT: '0x744d70FDBE2Ba4CF95131626614a1763DF805B9E',
    SPANK: '0x42d6622deCe394b54999Fbd73D108123806f6a18',
    TUSD: '0x8dd5fbCe2F6a956C3022bA3663759011Dd51e73E',
    ZRX: '0xE41d2489571d322189246DaFA5ebDe1F4699F498',
  },
  goerli: {
    ETH: 'ETH',
    WETH: WETH9[5].address,
    WINE: '0x86AcF69C2033174F944588FCD045a29717672e78',
    UNIWINE: '0xf0eb98A6eAba7eCA64ab4b247Ac5512b9c1f27AA',
    DAI: '0xdc31ee1784292379fbb2964b3b9c4124d8f89c60',
  },
  hardhat: {
    ETH: 'ETH',
    // WETH: WETH9[5].address,
    WINE: '0x86AcF69C2033174F944588FCD045a29717672e78',
    UNIWINE: '0x523772FA848073CdF0Ee94538Cc41D0475Ba51a9',
    // DAI: '0xdc31ee1784292379fbb2964b3b9c4124d8f89c60',
  },
  rinkeby: {
    ETH: 'ETH',
    WETH: WETH9[4].address,
    WINE: '0xbF24E7E2418366398c61AdE6026F4F4D7cA4C8Fc',
    UNIWINE: '0x26247F80826F9AaF842Fa8A6911D8C69f9C3eb8d',
    SOCKS: '0x23B608675a2B2fB1890d3ABBd85c5775c51691d5',
    DAI: '0xc7ad46e0b8a400bb3c915120d284aafba8fc4735',
    DAI2: '0x6B175474E89094C44Da98b954EedeAC495271d0F',
  },
}

export { ERC20_ABI, UNIWINE_ABI }

export const ERROR_CODES: { [key: string]: String } = [
  'INVALID_AMOUNT',
  'INVALID_TRADE',
  'INSUFFICIENT_ETH_GAS',
  'INSUFFICIENT_SELECTED_TOKEN_BALANCE',
  'INSUFFICIENT_ALLOWANCE',
].reduce((o, k) => {
  o[k] = k
  return o
}, {})

export const MODAL_TYPES: {
  [key: string]: string
} = ['BUY', 'SELL', 'CLAIM', 'REDEEM'].reduce((o, k) => {
  o[k] = k
  return o
}, {})

export function explorerUrlForTx (networkName, tx) {
  return `${EXPLORER_URLS[networkName]}/tx/${tx}`
}

export function isAddress (value) {
  try {
    ethers.utils.getAddress(value)
    return true
  } catch {
    return false
  }
}

export interface WineToken {
  id: number
  shipped: boolean
  metaURL: string
}
export interface WineTokenMeta {
  image: string
  attributes?: [{trait_type: string, value: string}]
}

// account is not optional
export function getSigner (library: Web3Provider, account: string): JsonRpcSigner {
  return library.getSigner(account).connectUnchecked()
}

// account is optional
export function getProviderOrSigner (library: Web3Provider, account?: string): Web3Provider | JsonRpcSigner {
  return account ? getSigner(library, account) : library
}

// account is optional
export function getContract (address: string, ABI: any, library: Web3Provider, account?: string): Contract {
  if (!isAddress(address) || address === AddressZero) {
    throw Error(`Invalid 'address' parameter '${address}'.`)
  }

  return new Contract(address, ABI, getProviderOrSigner(library, account) as any)
}

export function getTokenContract (tokenAddress, library, account) {
  return getContract(tokenAddress, ERC20_ABI, library, account)
}

// get the ether balance of an address
export async function getEtherBalance (address, library) {
  if (!isAddress(address)) {
    throw Error(`Invalid 'address' parameter '${address}'`)
  }

  const result = await library.getBalance(address)
  // console.debug(`getBalance(${address}):`, result)
  return result
}

// get the token balance of an address
export async function getTokenBalance (tokenAddress, address, library) {
  if (!isAddress(tokenAddress) || !isAddress(address)) {
    throw Error(`Invalid 'tokenAddress' or 'address' parameter '${tokenAddress}' or '${address}'.`)
  }
  return getContract(tokenAddress, ERC20_ABI, library).balanceOf(address)
}

// get the token balance of an address
export async function getWineTokenList (tokenAddress, address, library): Promise<WineToken[]> {
  if (!isAddress(tokenAddress) || !isAddress(address)) {
    throw Error(`Invalid 'tokenAddress' or 'address' parameter '${tokenAddress}' or '${address}'.`)
  }
  const Contract = getContract(tokenAddress, UNIWINE_ABI, library)
  const balance = await Contract.balanceOf(address)
  const tokens: WineToken[] = []
  for (let i = 0; i < balance; i++) { // i = index relative to owner
    const id = await Contract.tokenOfOwnerByIndex(address, i) as BigNumber // id = global index
    tokens.push(await getWineToken(Contract, id.toNumber()))
  }
  console.debug('getWineTokenList result', tokens)
  return tokens
}

export async function getWineToken (Contract: Contract, id: number): Promise<WineToken> {
  return {
    id: id,
    shipped: await Contract.isShipped(id),
    metaURL: await Contract.tokenURI(id),
  }
}

export async function getTokenAllowance (account, tokenAddress, spenderAddress, library) {
  if (!isAddress(account) || !isAddress(tokenAddress) || !isAddress(spenderAddress)) {
    throw Error(
      "Invalid 'address' or 'tokenAddress' or 'spenderAddress' parameter"
      + `'${account}' or '${tokenAddress}' or '${spenderAddress}'.`,
    )
  }

  return getContract(tokenAddress, ERC20_ABI, library).allowance(account, spenderAddress)
}

// get the token balance of an address
export async function requestWalletTokenAdd (library: Web3Provider, token: Token) {
  if (!library?.provider?.request) return false
  const request = {
    method: 'wallet_watchAsset',
    params: {
      type: /*  token.decimals === 0 ? 'ERC721' :  */'ERC20', // Only ERC20 supported
      options: {
        address: token.address,
        symbol: token.symbol,
        decimals: token.decimals,
        // image: 'https://placekitten.com/400/400',
      },
    },
  }
  console.log('[requestWalletTokenAdd]', request)
  // @ts-expect-error - typings are wrong here (params must be object, not array)
  return await library.provider.request(request)
}

export function amountFormatter (amount: TokenAmount | Price | BigNumber, baseDecimals = 18, displayDecimals = 3, useLessThan = true): string | undefined {
  try {
    // console.log('amountFormatter', amount)
    if (baseDecimals > 18 || displayDecimals > 18 || displayDecimals > baseDecimals) {
      throw Error(`Invalid combination of baseDecimals '${baseDecimals}' and displayDecimals '${displayDecimals}.`)
    }

    if (amount instanceof TokenAmount) { return displayDecimals === 0 ? amount.toFixed(0) : amount.toSignificant(displayDecimals) }
    if (amount instanceof Price) { return displayDecimals === 0 ? amount.toFixed(0) : amount.toSignificant(displayDecimals) }

    if (amount === undefined || amount === null) { // if balance is falsy, return undefined
      return undefined
    } else if (amount.isZero()) { // if amount is 0, return
      return '0'
    } else { // amount > 0
      // amount of 'wei' in 1 'ether'
      const baseAmount = BigNumber.from(10).pow(BigNumber.from(baseDecimals))

      const minimumDisplayAmount = baseAmount.div(
        BigNumber.from(10).pow(BigNumber.from(displayDecimals)),
      )

      // if balance is less than the minimum display amount
      if (amount.lt(minimumDisplayAmount)) {
        return useLessThan
          ? `<${ethers.utils.formatUnits(minimumDisplayAmount, baseDecimals)}`
          : `${ethers.utils.formatUnits(amount, baseDecimals)}`
      } else { // if the balance is greater than the minimum display amount
        const stringAmount = ethers.utils.formatUnits(amount, baseDecimals)

        // if there isn't a decimal portion
        if (stringAmount.match(/\./) == null) {
          return stringAmount
        } else { // if there is a decimal portion
          const [wholeComponent, decimalComponent] = stringAmount.split('.')
          const roundUpAmount = minimumDisplayAmount.div(ethers.constants.Two)
          const roundedDecimalComponent = BigNumber.from(decimalComponent.padEnd(baseDecimals, '0'))
            .add(roundUpAmount)
            .toString()
            .padStart(baseDecimals, '0')
            .substring(0, displayDecimals)

          // decimals are too small to show
          if (roundedDecimalComponent === '0'.repeat(displayDecimals)) {
            return wholeComponent
          } else { // decimals are not too small to show
            return `${wholeComponent}.${roundedDecimalComponent.toString().replace(/0*$/, '')}`
          }
        }
      }
    }
  } catch (error) {
    console.error('Failed to format amount', { amount, baseDecimals, displayDecimals, useLessThan })
    throw error
  }
}

export function parseTradeAmount (numberOfWINE: any) {
  let parsedValue
  try {
    parsedValue = ethers.utils.parseUnits(numberOfWINE, 18)
  } catch (error) {
    error.code = ERROR_CODES.INVALID_AMOUNT
    throw error
  }
  return parsedValue
}

export function makePromise () {
  let resolver
  const p = new Promise(resolve => { resolver = resolve })
  return [p, resolver]
}
export async function sleep (time) {
  return await new Promise(resolve => setTimeout(resolve, time))
}

export function fractionToBigNumber (fraction: Fraction): BigNumber {
  // throw new Error("it's getting late, I can't do this")
  // console.log("priceToBigNumber", price, ethers.utils.parseEther(price.toSignificant(18)).toString())
  return ethers.utils.parseEther(fraction.toSignificant(18)) // TODO: there no better way?
}

export function formatAddressShort (addr: string): string {
  return `0x${addr.slice(2, 5).toUpperCase()}...${addr.slice(-3).toUpperCase()}`
}
