import { Web3Provider } from '@ethersproject/providers'
import { InjectedConnector } from '@web3-react/injected-connector'
import { NetworkConnector } from '@web3-react/network-connector'
// import { WalletLinkConnector } from '@web3-react/walletlink-connector'
import { PortisConnector } from '@web3-react/portis-connector'
import { TorusConnector } from '@web3-react/torus-connector'
import { WalletConnectConnector } from '@web3-react/walletconnect-connector'
// import { FortmaticConnector } from './Fortmatic'
// import { NetworkConnector } from './NetworkConnector'

const NETWORK_URL = process.env.REACT_APP_PROVIDER_URL
// const FORMATIC_KEY = process.env.REACT_APP_FORTMATIC_KEY
// const PORTIS_ID = process.env.REACT_APP_PORTIS_ID
const WALLETCONNECT_BRIDGE_URL = process.env.REACT_APP_WALLETCONNECT_BRIDGE_URL ?? 'https://bridge.walletconnect.org'
const PORTIS_DAPP_ID = process.env.REACT_APP_PORTIS_DAPP_ID

export const NETWORK_NAME = process.env.REACT_APP_NETWORK_NAME ?? 'homestead'
export const NETWORK_CHAIN_ID: number = parseInt(process.env.REACT_APP_CHAIN_ID ?? '1')

if (typeof NETWORK_URL === 'undefined') {
  throw new Error('REACT_APP_PROVIDER_URL must be a defined environment variable')
}
const RPC_URLS = { [NETWORK_CHAIN_ID]: NETWORK_URL }

export const network = new NetworkConnector({
  urls: RPC_URLS,
})

export const injected = new InjectedConnector({
  supportedChainIds: [1, 3, 4, 5, 42, 1337],
})

let networkLibrary: Web3Provider | undefined
export function getNetworkLibrary (): Web3Provider {
  const provider = network.getProvider()
  return (networkLibrary = networkLibrary ?? new Web3Provider(provider as any))
}

export const walletconnect = new WalletConnectConnector({
  chainId: NETWORK_CHAIN_ID,
  rpc: RPC_URLS,
  bridge: WALLETCONNECT_BRIDGE_URL,
  qrcode: true,
  // pollingInterval: 15000, - apparently removed?
})

export const torus = new TorusConnector({
  chainId: NETWORK_CHAIN_ID,
  constructorOptions: {
    buttonPosition: 'bottom-right',
  },
  initOptions: {
    showTorusButton: false,
    network: {
      chainId: NETWORK_CHAIN_ID,
      host: NETWORK_NAME,
    },
  },
})
export const portis = new PortisConnector({
  dAppId: PORTIS_DAPP_ID ?? '',
  networks: [1, 3, 4, 5],
})

// mainnet only
// export const walletlink = new WalletLinkConnector({
//   url: NETWORK_URL,
//   appName: 'Uniswap',
//   appLogoUrl: UNISWAP_LOGO_URL
// })
