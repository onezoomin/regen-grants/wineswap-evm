import { MinusSmIcon, PlusIcon } from '@heroicons/react/solid'

import { extendClasses } from '../utils/react-utils'
import { useCount as useAppContextCount } from './Modal'

export default function IncrementToken ({ className, initialValue, max }) {
  const [count, incrementCount, decrementCount] = useAppContextCount(initialValue, max)

  const defaultClasses = 'flex items-stretch justify-between rounded text-white border-2 border-white w-max-content font-semibold text-xl select-none'
  const buttonClasses = 'h-auto w-5 box-content px-1 flex flex-col justify-center cursor-pointer'
  return (
    <div className={extendClasses(defaultClasses, className)} style={{ height: '36px' }}>
      <div className={buttonClasses} onClick={decrementCount}>
        <MinusSmIcon className="h-5" />
      </div>

      <div className="font-medium flex flex-col justify-center px-1">{count}</div>

      <span className={buttonClasses} onClick={incrementCount}>
        <PlusIcon className="h-5" />
      </span>
    </div>
  )
}
