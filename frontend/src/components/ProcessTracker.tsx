import Tooltip from '@material-ui/core/Tooltip'
import { Cancel, Done, Help, HourglassEmpty, SpeakerNotesOutlined, WarningOutlined } from '@material-ui/icons'
import capitalize from 'lodash/capitalize'
import last from 'lodash/last'
import { FunctionComponent } from 'react'
import { useDetectNetwork } from '../hooks/main'
import { Process, Step } from '../hooks/process'
import { formatError } from '../utils/error'
import { isWalletCancelError } from '../utils/web3-utils'
import { EtherscanLink } from './EtherscanLink'

export const ProcessTracker: FunctionComponent<{process: Process, className?: string }> = ({ process, className }) => {
  const { networkName } = useDetectNetwork()
  const icon = (step: Step) => {
    if (step.status === 'prompt') {
      return (
        <div className="flex flex-row items-center">
          <SpeakerNotesOutlined />
        </div>)
    }
    if (step.status === 'wait') {
      return (
        <div className="flex flex-row items-center">
          <HourglassEmpty />
        </div>)
    }
    if (step.status === 'success') return <Done />
    // if (step.status === 'wallet-error') return <SmsFailedOutlined /> -- currently not detected specifically
    if (step.status === 'error') {
      if (isCancelled(step)) { return <Cancel/> }
      return <WarningOutlined />
    }
    return <Help />
  }

  if (!process.steps.length) return null
  return (<div className={`${className ?? ''} w-full flex flex-col items-center`}>
    {last(process.steps).info ? <p>{last(process.steps).info}</p> : null}
    <ul className="w-full max-w-xxs my-4 flex flex-col justify-stretch">
      {process.steps.map((step: Step) => {
        const finished = step.status === 'success' || step.status.endsWith('error')

        let classes = 'flex justify-between'
        classes += (!finished
          ? ' animate-pulse duration-500'
          : (step.status.endsWith('error')
              ? (isCancelled(step) ? ' text-yellow-500' : ' text-red-500')
              : ' text-green-500'))

        let tooltip
        if (step.error) tooltip = formatError(step.error)
        if (isCancelled(step)) tooltip = 'Cancelled'
        // if (tooltip) classes += ' cursor-help' - doesn't look nice :/

        return (
          <Tooltip
            key={`${step.type}|${step.status}|${step.hash ?? 'no-trx'}`}
            title={tooltip ?? '' /* will not show for empty string */}
          >
            <li
              className={classes}
            >
              <span className="mr-4">{capitalize(step.type)}</span>
              <div className="flex flex-row items-center">
                {step.hash
                  ? <EtherscanLink className="text-sm text-white px-1 rounded-sm no-underline" style={{ borderWidth: '1px' }} trx={step.hash} networkName={networkName}>
                    TRX
                  </EtherscanLink>
                  : null}
                {icon(step)}
              </div>
            </li>
          </Tooltip>)
      })}
    </ul></div>
  )
}

function isCancelled (step: Step) {
  return isWalletCancelError(step.error)
}
