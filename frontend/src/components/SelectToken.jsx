import { Listbox } from '@headlessui/react'
import { SelectorIcon } from '@heroicons/react/solid'
import { Fragment } from 'react'
import { useDetectNetwork } from '../hooks/main'

export default function SelectToken ({ selectedTokenSymbol, setSelectedTokenSymbol, prefix, ...rest }) {
  const { addresses } = useDetectNetwork()
  const options = !addresses
    ? []
    : Object.keys(addresses)
      .filter(s => !['WINE', 'UNIWINE'].includes(s))
      .map(s => ({ value: s, label: s }))
  rest.className += ' relative'
  return (
    <div {...rest}>
      {/* <SelectMenu
        onChange={e => {
          setSelectedTokenSymbol(e.target.value)
        }}
        className="dropdown text-gray-50 min-w-fit"
      >
        {options.map((item, i) => renderOptions(item, i, selectedTokenSymbol, prefix))}
      </SelectMenu>

      <NoHeight>
        <DropControl src={arrow} alt="dropdown-arrow" />
      </NoHeight> */}

      <Listbox value={selectedTokenSymbol} onChange={setSelectedTokenSymbol}>
        <Listbox.Button
          className="relative pr-8 text-left text-white whitespace-nowrap focus:outline-none focus-visible:ring-2 focus-visible:ring-opacity-75 focus-visible:ring-white focus-visible:ring-offset-orange-300 focus-visible:ring-offset-2 focus-visible:border-indigo-500"
        >
          {prefix + ' ' + selectedTokenSymbol}
          <span className="absolute inset-y-0 right-0 flex items-center pointer-events-none">
            <SelectorIcon
              className="w-5 h-5 text-gray-400"
              aria-hidden="true"
            />
          </span>
        </Listbox.Button>
        <Listbox.Options
          className="absolute py-1 mt-1 w-full overflow-auto text-base bg-white rounded-md shadow-lg max-h-60 ring-1 ring-black ring-opacity-5 focus:outline-none"
          style={{ bottom: '2em' }}
        >
          {options.map((token) => (
            <Listbox.Option
              key={token.value}
              value={token.value}
              as={Fragment}
            >
              {({ active }) => (
                <li
                  className={`whitespace-nowrap p-2 ${
                    active ? 'bg-blue-500 text-white' : 'bg-white text-black'
                  }`}
                >
                  {token.label}
                </li>
              )}
            </Listbox.Option>
          ))}
        </Listbox.Options>
      </Listbox>
    </div>
  )
}
