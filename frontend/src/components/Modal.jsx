import { Transition } from '@headlessui/react'
import { Close } from '@material-ui/icons'
import last from 'lodash/last'
import { forwardRef, useEffect, createContext, useState } from 'react'
import Confetti from 'react-dom-confetti'
import { useSwipeable } from 'react-swipeable'
import { useAppContext } from '../context'
import { useEthContext } from '../context/eth-data'
import { useOverflowCheck } from '../hooks/misc'
import { MODAL_TYPES } from '../utils/main-utils'
import { extendClasses, rIF } from '../utils/react-utils'
import BuyAndSell from './BuyAndSell'
import Claim from './Claim'
import Confirmed from './Confirmed'
import Connect from './Connect'
import Redeem from './Ship'

const config = {
  angle: 270,
  spread: 150,
  startVelocity: 10,
  elementCount: 137,
  dragFriction: 0.04,
  duration: 7000,
  stagger: 4,
  width: '10px',
  height: '10px',
  colors: ['#A82548'/* '#a864fd', '#29cdff', '#78ff44', '#ff718d', '#fdff6a' */],
}

const ModalBackground = forwardRef(function ModalBackground ({ className = '', children, ...restProps }, ref) {
  const defaultClasses = `fixed top-0 left-0 w-screen h-screen bg-black opacity-75 bg-black ${className}`
  return (
    <div id="ModalBG" ref={ref} className={extendClasses(defaultClasses, className)} {...restProps}>
      {children}
    </div>
  )
})

const ModalClose = ({ swipeMode, onClose }) => {
  let classes = 'w-full flex absolute top-0'
  classes += swipeMode ? ' justify-center' : ' justify-end'
  return (
    <div id="ModalClose" className={classes}>
      {swipeMode
        ? <div className="w-12 mt-3 h-1 rounded-full bg-white" style={{ boxShadow: '0px 0px 3px black' }}></div>
        : <div className="p-2 text-white drop-shadow-lg cursor-pointer" style={{ textShadow: '0px 0px 3px black' }} onClick={onClose}><Close /></div>}
    </div>
  )
}

export function useCount (initialValue, max) {
  const { appState, setAppState } = useAppContext()

  const increment = () => {
    setAppState(state => {
      const newCount = state.count + 1
      if (!max || newCount <= max) {
        return { ...state, count: newCount }
      } else {
        return state
      }
    })
  }
  const decrement = () => appState.count > 1 && setAppState(state => ({ ...state, count: state.count - 1 }))
  const setCount = (val) => setAppState(state => ({ ...state, count: val }))

  // ok to disable exhaustive-deps for `setState` b/c it's actually just a useState setter
  useEffect(() => {
    if (initialValue) {
      setAppState(state => ({ ...state, count: initialValue }))
    }
  }, [initialValue]) // eslint-disable-line react-hooks/exhaustive-deps

  return [appState.count, increment, decrement, setCount]
}

export const ModalContext = createContext({ contentRef: null })

export default function Modal ({
  setShowConnect,
  showConnect,
  ethData,
}) {
  const { appState, setAppState } = useAppContext()
  const { process } = useEthContext()

  const [disableClose, setDisableClose] = useState(false)
  const { hasScrollOverflow, ref: contentRef } = useOverflowCheck([appState.visible])
  const [confetti, setConfetti] = useState(false)

  // http://www.stucox.com/blog/you-cant-detect-a-touchscreen/#pointer-media-queries
  const isDeviceWithPointer = window.matchMedia('(pointer: coarse)').matches

  const maybeCloseModal = () => !disableClose && closeModal()
  const swipeHandlers = useSwipeable({
    // onSwiped: (eventData) => console.log("User Swiped!", eventData),
    onSwipedDown: () => {
      if (!hasScrollOverflow) {
        maybeCloseModal()
      }
    },
    delta: 10, // min distance(px) before a swipe starts
    preventDefaultTouchmoveEvent: false, // call e.preventDefault *See Details*
    trackTouch: true, // track touch input
    trackMouse: true, // track mouse input
    rotationAngle: 0, // set a rotation angle
  })

  // disable modal close during process
  useEffect(() => {
    setDisableClose(process?.active)
  }, [process?.active, setDisableClose])

  function closeModal () {
    if (appState.visible) {
      setAppState(state => ({ ...state, visible: !state.visible, count: 1 }))
      setConfetti(false)
      process.reset()
    }
    setShowConnect(false)
  }

  // console.log('Modal', appState, process)
  function renderContent () {
    if (showConnect) {
      return <Connect setShowConnect={setShowConnect} closeModal={closeModal} />
    } else if ([MODAL_TYPES.BUY, MODAL_TYPES.SELL].includes(appState.tradeType) && process?.success) {
      return (
        <Confirmed
          ethData={ethData}
          type={appState.tradeType}
          hash={last(process.steps).hash}
          amount={appState.count}
          closeModal={closeModal}
          clearLastTransaction={() => {
            process.reset()
          }}
        />
      )
    } else {
      if ([MODAL_TYPES.BUY, MODAL_TYPES.SELL].includes(appState.tradeType)) {
        return (
          <BuyAndSell
            setShowConnect={setShowConnect}
          />
        )
      } else if (appState.tradeType === MODAL_TYPES.CLAIM) {
        return (
          <Claim
            setDisableClose={setDisableClose}
          />
        )
      } else {
        return (
          <Redeem
            setConfetti={setConfetti}
            setDisableClose={setDisableClose}
          />
        )
      }
    }
  }

  // const isInvisible = !state.visible && !showConnect ? 'hidden ' : 'visible '
  return (
    <Transition
      show={appState.visible || showConnect}
      className={'w-full h-full flex flex-col items-center justify-end pointer-events-auto'}
    >
      {/* Backdrop to darken rest of the page */}
      <Transition.Child
        className="absolute inset-0 w-full h-full"
        enter="transition-opacity ease-linear duration-300"
        enterFrom="opacity-30"
        enterTo="opacity-100"
        leave="transition-opacity ease-linear duration-200"
        leaveFrom="opacity-100"
        leaveTo="opacity-30"
      >
        <ModalBackground
          {...swipeHandlers}
          onClick={maybeCloseModal}
        />
        <div className="fixed z-1 inset-0 w-screen h-screen flex justify-center overflow-hidden pointer-events-none">
          <Confetti className="flex-none" active={confetti} config={config} />
        </div>
      </Transition.Child>
      <Transition.Child
        className="z-10 w-full max-w-md"
        style={showConnect ? { maxWidth: '20em' } : null}
        enter="duration-300 transition-transform transform"
        enterFrom="translate-y-full"
        enterTo="translate-y-0"
        leave="duration-200 transition-transform transform"
        leaveFrom="translate-y-0"
        leaveTo="translate-y-full"
      >
        <div id="ModalFrame" {...swipeHandlers}
          className={'max-h-screen w-full rounded-t-lg bg-grey-850 relative flex flex-col overflow-hidden'}
        >
          <ModalContext.Provider value={{ contentRef }}>
            {renderContent()}
          </ModalContext.Provider>
          {rIF(!disableClose, (<ModalClose swipeMode={
            isDeviceWithPointer
            && !hasScrollOverflow
          } onClose={closeModal}
          />))}
        </div>
      </Transition.Child>
    </Transition>
  )
}
