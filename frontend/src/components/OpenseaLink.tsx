import { Token } from '@uniswap/sdk-core'
import { AnchorHTMLAttributes, FunctionComponent } from 'react'
import { LinkOut } from './minis'

export const OpenseaLink: FunctionComponent<AnchorHTMLAttributes<HTMLAnchorElement> & {
  token: Token
  tokenID: Number
}> = ({ token, tokenID, children, ...restProps }) => {
  const baseUri = token.chainId === 1 ? 'https://opensea.io' : 'https://testnets.opensea.io'
  return <LinkOut
    title="View token on Opensea"
    href={token ? `${baseUri}/assets/${token.address}/${tokenID}` : ''}
    {...restProps}
  >
    {children ?? (<>
      View token on Opensea
      {/* <OpenInNew style={{ marginLeft: '0.2em', height: '0.7em', width: '0.7em' }}/> */}
    </>)}
  </LinkOut>
}
