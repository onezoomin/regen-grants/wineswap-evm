import { Web3Provider } from '@ethersproject/providers'
import { Percent, Token, TokenAmount, TradeType } from '@uniswap/sdk-core'
import { Route, Trade } from '@uniswap/v2-sdk'
import { BigNumber, Contract, ethers } from 'ethers'
import { fractionToBigNumber } from '../utils/main-utils'

// denominated in bips
const GAS_MARGIN = BigNumber.from(1000)

// denominated in bips
export const ALLOWED_SLIPPAGE_PERCENT = new Percent('3', '100') // TODO: allowed slippage
export const ALLOWED_SLIPPAGE = BigNumber.from(200)

export function calculateGasMargin (value, margin = GAS_MARGIN) {
  const offset = value.mul(margin).div(BigNumber.from(10000))
  return value.add(offset)
}
export async function estimateGasPrice (library) {
  const gasPrice = await library.getGasPrice()
  return gasPrice// .mul(BigNumber.from(150)).div(BigNumber.from(100)) // TODO: why?
}

export function calculateSlippageBounds (trade: Trade) {
  return {
    maximumAmountIn: fractionToBigNumber(trade.maximumAmountIn(ALLOWED_SLIPPAGE_PERCENT)),
    minimumAmountOut: fractionToBigNumber(trade.minimumAmountOut(ALLOWED_SLIPPAGE_PERCENT)),
  }
}

// this mocks the getInputPrice function, and calculates the required output
export function calculateEtherTokenOutputFromInput (inputAmount, inputReserve, outputReserve) {
  const inputAmountWithFee = inputAmount.mul(BigNumber.from(997))
  const numerator = inputAmountWithFee.mul(outputReserve)
  const denominator = inputReserve.mul(BigNumber.from(1000)).add(inputAmountWithFee)
  return numerator.div(denominator)
}

// this mocks the getOutputPrice function, and calculates the required input
export function calculateEtherTokenInputFromOutput (outputAmount, inputReserve, outputReserve) {
  const numerator = inputReserve.mul(outputAmount).mul(BigNumber.from(1000))
  const denominator = outputReserve.sub(outputAmount).mul(BigNumber.from(997))
  return numerator.div(denominator).add(ethers.constants.One)
}

// get exchange rate for a token/ETH pair
export function getExchangeRate (inputValue, outputValue, invert = false) {
  const inputDecimals = 18
  const outputDecimals = 18

  if (inputValue && inputDecimals && outputValue && outputDecimals) {
    const factor = BigNumber.from(10).pow(BigNumber.from(18))

    if (invert) {
      return inputValue
        .mul(factor)
        .div(outputValue)
        .mul(BigNumber.from(10).pow(BigNumber.from(outputDecimals)))
        .div(BigNumber.from(10).pow(BigNumber.from(inputDecimals)))
    } else {
      return outputValue
        .mul(factor)
        .div(inputValue)
        .mul(BigNumber.from(10).pow(BigNumber.from(inputDecimals)))
        .div(BigNumber.from(10).pow(BigNumber.from(outputDecimals)))
    }
  }
}

export function calculateTrade (
  route: Route,
  tokenAmountWei: BigNumber,
  TOKEN: Token,
): Trade {
  console.debug('Calculating price for route:', route, ethers.utils.formatEther(tokenAmountWei), { in: route.input === TOKEN, out: route.output === TOKEN })
  const trade = new Trade(
    route,
    new TokenAmount(TOKEN, tokenAmountWei.toString()),
    route.input === TOKEN ? TradeType.EXACT_INPUT : TradeType.EXACT_OUTPUT,
  )
  console.debug('Calculated trade:', {
    inputAmount: trade.inputAmount.toSignificant(6),
    outputAmount: trade.outputAmount.toSignificant(6),
    executionPrice: trade.executionPrice.toSignificant(6),
    nextMidPrice: trade.nextMidPrice.toSignificant(6),
    // slippage: trade.slippage,
    trade,
  })
  return trade
}

// approve spending of a token by a contract
export async function requestApproval (library: Web3Provider, tokenContract: Contract, spenderAddress: string, amount: BigNumber) {
  console.debug('[requestApproval]', { tokenContract, spenderAddress, amount: amount.toString() })
  const estimatedGasLimit = await tokenContract.estimateGas.approve(spenderAddress, amount)
  const estimatedGasPrice = await library
    .getGasPrice()
    .then(gasPrice => gasPrice.mul(BigNumber.from(150)).div(BigNumber.from(100)))

  return tokenContract.approve(spenderAddress, amount, {
    gasLimit: calculateGasMargin(estimatedGasLimit),
    gasPrice: estimatedGasPrice,
  })
}
